extern crate byteorder;


use std::env;
use std::fs::File;
use std::io::prelude::*;
use byteorder::{ByteOrder, BigEndian};
use std::str;
use std::io::Result;

fn main() {
    //println!("Hello, world! {}", env::args[0]);

    let mut args = env::args();

    if args.len() < 2 {
        println!("Specify patch file");
        return;
    }


    let filename_opt = args.nth(1);

    match filename_opt {
        Some(filename) => read_file(filename),
        None => println!("Specify patch file FUK"),
    }
}


fn read_file(filename: String) {
    let mut file = File::open(filename).expect("file not found");

    println!("{:?}", file);


    read_form_chunk(&mut file);
}


fn read_form_chunk<F: Read + Seek>(reader: &mut F) {
    let mut form: [u8; 4] = [0; 4];
    reader.read(&mut form);
    println!("Form: {:?}", str::from_utf8(&form).unwrap());

    let mut ck_size_bytes: [u8; 4] = [0; 4];
    reader.read(&mut ck_size_bytes);
    let ck_size: i32 = BigEndian::read_i32(&ck_size_bytes);
    println!("Form chunk size {:?}", ck_size);


    let mut form_type: [u8; 4] = [0; 4];
    reader.read(form_type.as_mut());
    println!("Form type: {:?}", str::from_utf8(&form_type).unwrap());



    //println!("{:?}", reader.seek(std::io::SeekFrom::End(0)));


    let mut need_to_read = true;

    while need_to_read {

        need_to_read = read_chunk(reader);
        if !need_to_read {
            break;
        }
    }
}

fn read_result(result: Result<usize>) -> bool {
    match result {
        Ok(bytes_size) => {
            if bytes_size > 0 {
                true
            } else {
                false
            }
        },
        Err(e) => false,
    }
}

fn read_chunk<F: Read>(reader: &mut F) -> bool {
    let mut chunk_type: [u8; 4] = [0; 4];
    let mut result = false;


    result = read_result(reader.read(&mut chunk_type));



    if !result {
        return result;
    }

    println!("Type: {:?}", str::from_utf8(&chunk_type).unwrap());

    let mut ck_size_bytes: [u8; 4] = [0; 4];

    result = read_result(reader.read(&mut ck_size_bytes));

    if !result {
        return result;
    }



    let ck_size: i32 = BigEndian::read_i32(&ck_size_bytes);
    println!("Chunk size {:?}", ck_size);


    let mut data = vec![0; ck_size as usize];


    result = read_result(reader.read(&mut data));

    if !result {
        return result;
    }

    match str::from_utf8(&data) {
        Ok(text) => println!("DATA: {:?}", text),
        Err(e) => println!("binary data"),
    }

    return result;
}
